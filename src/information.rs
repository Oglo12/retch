#![allow(dead_code)]

use fspp::*;

pub fn get_os() -> String {
    return match std::env::consts::OS {
        "linux" => linux_distro(),
        _ => pretty_os_name(),
    };
}

pub fn get_arch() -> String {
    return std::env::consts::ARCH.to_string();
}

fn pretty_os_name() -> String {
    return match std::env::consts::OS {
        "linux" => "Linux",
        "windows" => "Windows",
        "macos" => "MacOS",
        "ios" => "iOS",
        "freebsd" => "FreeBSD",
        "openbsd" => "OpenBSD",
        "netbsd" => "NetBSD",
        "dragonfly" => "DragonFly",
        "solaris" => "Solaris",
        "android" => "Android",
        _ => std::env::consts::OS,
    }.to_string();
}

fn linux_distro() -> String {
    let os_release_path = Path::new("/etc/os-release");

    return file::read(&os_release_path).unwrap()
        .trim()
        .split("\n")
        .filter(|x| x.starts_with("NAME="))
        .map(|x| x.replace("\"", ""))
        .map(|x| x.replace("NAME=", ""))
        .collect::<Vec<String>>()[0]
        .to_string();
}

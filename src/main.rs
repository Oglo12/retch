mod information;

use colored::Colorize;
use sysinfo::{System, SystemExt};
use fspp::*;

const DEFAULT_CONFIG: &str = "os
kernel
arch
username
hostname
program_version
";

struct Map(Vec<(String, String)>);

impl Map {
    pub fn new() -> Self {
        return Self(Vec::new());
    }

    pub fn insert(&mut self, key: String, value: String) {
        self.0.push((key, value));
    }

    pub fn len(&self) -> usize {
        return self.0.len();
    }

    pub fn iter(&self) -> std::slice::Iter<'_, (String, String)> {
        return self.0.iter();
    }
}

fn init_config() {
    let config_file_path = config_path().add_str("config.txt");

    match directory::create(&config_path()) {
        Ok(_) => {
            if config_file_path.exists() == false {
                file::write(DEFAULT_CONFIG, &config_file_path).unwrap_or(());
            }
        },
        Err(_) => (),
    };
}

fn main() {
    let mut system = System::new_all();
    system.refresh_all();

    let mut info = Map::new();

    let config_file_path = config_path().add_str("config.txt");

    init_config();

    let config: Vec<String> = file::read(&config_file_path).unwrap_or(DEFAULT_CONFIG.into()).trim().split("\n").map(|x| x.to_string()).collect();

    for i in config {
        match i.as_str() {
            "os" => info.insert("Operating System".into(), information::get_os()),
            "kernel" => info.insert("Kernel".into(), system.kernel_version().unwrap_or("< failed to get kernel version >".into())),
            "arch" => info.insert("Architecture".into(), information::get_arch()),
            "username" => info.insert("Username".into(), std::env::var("USER").unwrap_or("< failed to get username >".into())),
            "hostname" => info.insert("Hostname".into(), system.host_name().unwrap_or("< failed to get hostname >".into())),
            "program_version" => info.insert("Program Version".into(), clap::crate_version!().into()),
            _ => (),
        };
    }

    print_map(&info);
}

fn config_path() -> Path {
    return Path::new(&dirs::config_dir().unwrap().display().to_string()).add_str(clap::crate_name!());
}

fn print_map(map: &Map) {
    if map.len() == 0 {
        return;
    }

    let mut key_max_length: usize = 0;
    let mut value_max_length: usize = 0;

    for i in map.iter() {
        let key_len = i.0.chars().into_iter().collect::<Vec<char>>().len();
        let value_len = i.1.chars().into_iter().collect::<Vec<char>>().len();

        if key_len > key_max_length {
            key_max_length = key_len;
        }

        if value_len > value_max_length {
            value_max_length = value_len;
        }
    }

    let string_max_length = key_max_length + value_max_length + 7;

    for i in 0..string_max_length {
        if i == 0 {
            print!("{}", "┏".bright_black().bold());
        }

        else if i == string_max_length - 1 {
            print!("{}", "┓".bright_black().bold());
        }

        else if i == key_max_length + 3 {
            print!("{}", "┬".bright_black().bold());
        }

        else {
            print!("{}", "─".bright_black().bold());
        }
    }

    print!("\n");

    for i in map.iter() {
        let key_len = i.0.chars().into_iter().collect::<Vec<char>>().len();
        let value_len = i.1.chars().into_iter().collect::<Vec<char>>().len();

        let mut space_left = String::from(" ");
        let mut space_right = String::from(" ");

        for _ in 0..key_max_length - key_len {
            space_left.push_str(" ");
        }

        for _ in 0..value_max_length - value_len {
            space_right.push_str(" ");
        }

        println!("{line}{sl}{} {line} {}{sr}{line}", i.0.bright_cyan(), i.1.bright_green(), sl = space_left, sr = space_right, line = "│".bright_black().bold());
    }

    for i in 0..string_max_length {
        if i == 0 {
            print!("{}", "┗".bright_black().bold());
        }

        else if i == string_max_length - 1 {
            print!("{}", "┛".bright_black().bold());
        }

        else if i == key_max_length + 3 {
            print!("{}", "┴".bright_black().bold());
        }

        else {
            print!("{}", "─".bright_black().bold());
        }
    }

    print!("\n");
}
